import React, { useEffect } from 'react';

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('state');   // Get the select tag element by its id 'state', which is in index.html
      for (let state of data.states) {                      //For each state in the states property of the data, located here http://localhost:8000/api/states/.
        const option = document.createElement("option")              // Create an 'option' element
         option.value = state.abbreviation                   // Set the '.value' property of the option element to the state abbreviation
        option.innerHTML = state.name                       // Set the '.innerHTML' property of the option element to the state's name
        selectTag.appendChild(option)                       // Append the option element as a child of the select tag
      }
    }
  });

  window.addEventListener('DOMContentLoaded', async () => {


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async(event) => {    //why the parameter event is inside async
      event.preventDefault(); //console.log('need to submit the form data'); checks hat the console in http://localhost:3000/new-location.html? is getting the data
      const formData = new FormData(formTag);                       //transforms FormData(HTML5 object) to JSON
      const json = JSON.stringify(Object.fromEntries(formData)); //       console.log(json); will show {"Name":"Isaac Olivares","Room count":"100","City":"Round Rock"}

      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
      method: "post",
      body: json,
      headers: {
    'Content-Type': 'application/json',
  },
};
const response = await fetch(locationUrl, fetchConfig);
if (response.ok) {
  formTag.reset();
  const newLocation = await response.json(); // use console.log(newLocation); to look at
  console.log(newLocation)
}
    });
  });


  