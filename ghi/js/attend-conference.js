window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    // Here, add the 'd-none' class to the loading icon
      let spinner = document.querySelector('#loading-conference-spinner')
      spinner.classList.add('d-none')
      // Here, remove the 'd-none' class from the select tag
      let selector = document.querySelector('#conference')
      selector.classList.remove('d-none')
    }
});


window.addEventListener('DOMContentLoaded', async () => {


    const formTag = document.getElementById('create-attendee-form'); //id changed
    formTag.addEventListener('submit', async(event) => {    //why the parameter event is inside async
    event.preventDefault(); //console.log('need to submit the form data'); checks that the console in http://localhost:3000/new-location.html? is getting the data
    const formData = new FormData(formTag);                       //transforms FormData(HTML5 object) to JSON
    const json = JSON.stringify(Object.fromEntries(formData)); //       console.log(json); will show {"Name":"Isaac Olivares","Room count":"100","City":"Round Rock"}

    const locationUrl = 'http://localhost:8001/api/attendees/'; //url changed
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {
    'Content-Type': 'application/json',
  },
};

const response = await fetch(locationUrl, fetchConfig);
if (response.ok) {
  formTag.reset();
  const newAttendee = await response.json(); // use console.log(newLocation); to look at
  console.log(newAttendee)
}
    // Here, add the 'd-none' class to the loading icon
    let formView = document.getElementById('create-attendee-form')
    formView.classList.add('d-none')
    // Here, remove the 'd-none' class from the select tag
    let selector = document.getElementById('success-message')
    selector.classList.remove('d-none')
    });
  });
