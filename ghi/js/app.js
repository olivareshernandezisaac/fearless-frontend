function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card" p-3 mb-4 shadow>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
        </div>
      </div>
    `;
  }
/// RETURNS code in DD/MM/YYYY FORMAT in the browser

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/'

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw "bogus"
      } else {
        const data = await response.json()  ///console.log(data) after to check

        let index =0
        for (let conference of data.conferences){
            const datailUrl = `http://localhost:8000${conference.href}`; //we get inside href for conferenc 0.. http://localhost:8000/api/conferences/2/ a get methdo
            const detailResponse = await fetch(datailUrl)
            if(detailResponse.ok){
                const details = await detailResponse.json(); //console.log(details)
                const title = details.conference.name
                const description = details.conference.description; //console.log(description)to check.......conference comes from the first key,conference wne we are inside conferences/2/^^
                const pictureUrl = details.conference.location.picture_url // look at http://localhost:8000/api/conferences/2/ to see the json object and know how we go to picture
                const starts = details.conference.starts
                const ends = details.conference.ends
                const location = details.conference.location.name
                const html = createCard(title, description, pictureUrl, starts, ends, location, location)
                const column = document.querySelector(`#col-${index%3}`);  //it return remainder 0, 1, 2. then it goes to html and assigns it to each of those columns
                column.innerHTML += html;
                index +=1   // then it restarts with new column
            }
        }
      }
    }
    catch (e) {
        console.log('caught inner "bogus"'+ e)
    }
  });


// OLD WAY TO DO IT
// Only can do one card, we changed it ^^^ so it can take as many cards/conferences as possible

//   window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/'
//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         throw "bogus"
//       } else {
//         const data = await response.json()  ///console.log(data) after to check

//         const conference = data.conferences[0] //accesses first conference out of th earray and prints it in the console // console.log(conference) after this
//         const nameTag = document.querySelector('.card-title')
//         nameTag.innerHTML =conference.name // set the innerHTML property of the element to set the name of the conference to the content of that element.

//         const datailUrl = `http://localhost:8000${conference.href}`; //we get inside href for conferenc 0.. http://localhost:8000/api/conferences/2/ a get methdo
//         const detailResponse = await fetch(datailUrl)
//         if(detailResponse.ok){
//             const details = await detailResponse.json(); //console.log(details)
//             const description = details.conference.description; //console.log(description)to check.......conference comes from the first key,conference wne we are inside conferences/2/^^
//             const descriptionTag = document.querySelector('.card-text') // set the innerHTML property of the element to set the name of the description to the content of that element.
//             descriptionTag.innerHTML = description;
//                                                         //we added picture_url property to LocationListEncoder to use the image
//             console.log(details)
//             const imageTag = document.querySelector('.card-img-top')
//             imageTag.src = details.conference.location.picture_url // look at http://localhost:8000/api/conferences/2/ to see the json object and know how we go to picture
//         }
//       }
//     }
//     catch (e) {
//         console.log('caught inner "bogus"'+ e)
//     }
//   });
