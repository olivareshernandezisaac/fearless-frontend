import Nav from './Nav';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import {BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'


function App(props) {
  if(props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav/>
      {/* <div className="container"> */}
        <Routes>
          <Route index element={<MainPage/>} />
          <Route path="locations">
            <Route path="new" element={<LocationForm/>} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm/>} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm/>} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm/>} />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees}/>} />
          </Route>
        </Routes>
      {/* </div> */}
    </BrowserRouter>
    );
}

export default App;

//before we added routes  1/18
// function App(props) {
//   if(props.attendees === undefined){
//     return null;
//   }
//   return (
//   <>
//     <Nav/>
//     <div className="container">
//       <LocationForm/>
//         {/* <ConferenceForm /> */}
//         {/* <AttendConferenceForm /> */}
//         {/* <PresentationForm /> */}
//      {/* <AttendeesList attendees={props.attendees}/>  */} {/* passing attendees data from App componen*/}
//     </div>                                   {/* to the new AttendeeList component to get it to properly render */}
//   </>
//   );
// }

// export default App;








// old code changed 1/17
// function App(props) {
//   if(props.attendees === undefined){
//     return null;
//   }
//   return (
//   <>
//     <Nav/>
//     <div className="container">
//     <table className="table table-striped">
//     <thead>
//       <tr>
//         <th>Name</th>
//         <th>Conference</th>
//       </tr>
//     </thead>
//     <tbody>
//     {props.attendees.map(attendee => {
//       return (
//         <tr key={attendee.href}>
//           <td>{ attendee.name }</td>
//           <td>{ attendee.conference }</td>
//         </tr>
//       );
//     })}
//     </tbody>
//   </table>
//     </div>
//   </>
//   );
// }

// export default App;


//old code 1/13
//add a key so that React can keep track of the things it's generating

// 2. function App(props) {
//   if(props.attendees === undefined){
//     return null;
//   }
//   return (
// <div>
//       <table>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           for(let attendee of props.attendees){
//             <tr>
//               <td>{attendee.name}</td>
//               <td>{attendee.conference}</td>
//             </tr>
//           }
//         </tbody>
//       </table>
//     </div>
//   );
// }


// import logo from './logo.svg';    1
// import './App.css';

//Edit <code>src/App.js</code> and save to reload.
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           My code reloads
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
