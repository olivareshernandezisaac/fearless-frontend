import React, {useEffect, useState} from 'react';
                            {/* creating stateful component*/} {/* means that it can have internal data saved in the component when it gets created*/}
                            {/*in html we can have tags with not closing tags, in react everything has closing tags */}
                            {/* for is a reserved keyword, like class, so we change it to htmlFor*/}

function LocationForm() {

    const [states, setStates] = useState([]);        //useState() hook is used to set and change the state in a component."
                                                    //creates a variable states, to store data in the comoponent, creates method setStates, and then it sets the initial value empty array[]
    const [name, setName] = useState("");      // Set the useState hook to store "name" in the component's state,with a default initial value of an empty string.

    const handleNameChange = (event) =>{               // Create the handleNameChange method to take what the user inputs into the form and store it in the state's "name" variable.
        const value = event.target.value;              //event parameter is the event that occurred. target property is the HTML tag that caused the event. So, in this case, event.target is the user's input in the form for the location's name
        setName(value);
    }

    const [roomCount, setRoomCount] = useState('');

    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
      }

    const [city, setCity] = useState('');

    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const [state, setState] = useState('');

    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
        }

    const handleSubmit = async (event) =>{
        event.preventDefault();

        const data ={};  //we created this empty JSON object

        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json(); // use console.log(newLocation); to look at
    console.log(newLocation)

    setName('');
    setRoomCount("");
    setCity("");
    setState("");
  }
}




    const fetchData = async () =>{
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();  //gives us an array of all the states
        setStates(data.states)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form" >
            <div className="form-floating mb-3">
              <input value = {name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value = {roomCount} onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input value = {city} onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select value = {state}  onChange={handleStateChange}required name="state" id="state" className="form-select">
                <option value="">Choose a state</option>
                {states.map(s=> {
                    return (
                        <option key ={s.abbreviation} value={s.abbreviation}>
                            {s.name}
                        </option>
                    );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;


//notes
// onChange event handler keep track of the state of the different inputs in the form

// before we added useState
// nction LocationForm() {
//     const fetchData = async () =>{
//     const url = 'http://localhost:8000/api/states/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();

//       const selectTag = document.getElementById('state');   // Get the select tag element by its id 'state', which is in index.html
//       for (let state of data.states) {                      //For each state in the states property of the data, located here http://localhost:8000/api/states/.
//         const option = document.createElement("option")              // Create an 'option' element
//          option.value = state.abbreviation                   // Set the '.value' property of the option element to the state abbreviation
//         option.innerHTML = state.name                       // Set the '.innerHTML' property of the option element to the state's name
//         selectTag.appendChild(option)                       // Append the option element as a child of the select tag
//       }
//     }
//   }

//   useEffect(() => {
//     fetchData();
//   }, []);

//   return (
//     <div className="row">
//       <div className="offset-3 col-6">
//         <div className="shadow p-4 mt-4">
//           <h1>Create a new location</h1>
//           <form id="create-location-form">
//             <div className="form-floating mb-3">
//               <input placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
//               <label htmlFor="name">Name</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
//               <label htmlFor="room_count">Room count</label>
//             </div>
//             <div className="form-floating mb-3">
//               <input placeholder="City" required type="text" name="city" id="city" className="form-control"/>
//               <label htmlFor="city">City</label>
//             </div>
//             <div className="mb-3">
//               <select required name="state" id="state" className="form-select">
//                 <option value="">Choose a state</option>
//               </select>
//             </div>
//             <button className="btn btn-primary">Create</button>
//           </form>
//         </div>
//       </div>
//     </div>
//   );
// }

// export default LocationForm;




// this JSX will turn into an actual paragraph tag by react


// {/* 1/17 old Code
// import React from 'react';
//                             {/* creating stateful component**/}
// function LocationForm(){    {/* means that it can have internal data saved in the component when it gets created*/}
//     return (
//         <p>A location form</p> // this JSX will turn into an actual paragraph tag by react
//     );
// }

// export default LocationForm;
// */}
